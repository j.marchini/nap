#!/usr/bin/python3
"""
Created on 08 . 10 . 2018
@author: jerome
Dolibarr V7.02

"""

import json
import requests
import logging
from datetime import datetime, date
import time
from dateutil import tz
from enum import Enum
import re
# cli mode
import yaml
import argparse
import csv
import sys
# import copy

dol_api_key_token = "dolibarr_api_key"
server_token = "dolibarr_server"

# dol_timezone = pytz.UTC  # by default
dol_timezone = tz.gettz('Europe/Paris')


class DolibarrException(Exception):
    pass


class DolibarrRestException(DolibarrException):
    pass


class DolibarrRest:
    def __init__(self, logger, conf):
        self.base_URL = "https://{0}/"
        self.server = ""
        self.headers = {}
        self.server = conf[server_token]
        self.base_URL = self.base_URL.format(self.server) + "api/index.php/"
        self.headers["DOLAPIKEY"] = conf[dol_api_key_token]
        self.read_only = conf.get('read_only')
        if logger:
            self.logger = logger
        else:
            self._make_logger()

    def _make_logger(self):
        self.logger = logging.getLogger("")
        ch = logging.StreamHandler()
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)
        self.logger.setLevel(logging.INFO)

    # json facilities
    @staticmethod
    def json_obj_handler(obj):
        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        # enum is serialized as string, not integer, to be more readable
        if isinstance(obj, Enum):
            return obj.name
        raise DolibarrRestException('Class "%s" not defined for json serialization' % obj.__name__)

    # timestamp to date and datetime facilities
    @staticmethod
    def timestamp_to_datetime(timestamp):
        if timestamp is not None:
            return datetime.fromtimestamp(timestamp, tz=dol_timezone)
        else:
            return None

    @staticmethod
    def timestamp_to_date(timestamp):
        if timestamp is not None:
            return date.fromtimestamp(timestamp)
        else:
            return None

    @staticmethod
    def dolibarr_datetime_to_datetime(d):
        if d is not None:
            # this regex removes all colons and all
            # dashes EXCEPT for the dash indicating + or - utc offset for the timezone
            conformed_timestamp = re.sub(r"[:]|([-](?!((\d{2}[:]\d{2})|(\d{4}))$))", '', d)
            # remove milliseconds if any (<time>.mm..m)
            conformed_timestamp = re.sub('\..*$', '', conformed_timestamp)
            return datetime.strptime(conformed_timestamp, "%Y%m%d %H%M%S")
        else:
            return None

    @staticmethod
    def iso_to_datetime(d):
        if d is not None:
            # this regex removes all colons and all
            # dashes EXCEPT for the dash indicating + or - utc offset for the timezone
            conformed_timestamp = re.sub(r"[:]|([-](?!((\d{2}[:]\d{2})|(\d{4}))$))", '', d)
            return datetime.strptime(conformed_timestamp, "%Y%m%dT%H%M%S.%f%z")
        else:
            return None

    @staticmethod
    def datetime_to_iso(d):
        if d is not None:
            return d.isoformat()
        else:
            return None

    @staticmethod
    def datetime_to_dol_timestamp(d):
        if d is None:
            return 0
        else:
            return int(time.mktime(d.timetuple()))

    # Dolibarr facilities
    @staticmethod
    def get_extra_fields(d):
        """
        returns None or dict with extra field values and keys without technical prefix 'options_'
        :param d:
        :return:
        """
        if 'array_options' not in d:
            return None
        if d['array_options'] is None:
            return None
        if isinstance(d['array_options'], list):
            return None
        return {k.replace('options_', ''): d['array_options'][k] for k in d['array_options'].keys()}

    @staticmethod
    def get_extra_field(d, field_name, default_value=None):
        """
            get extra field value from dict representation of a Dolibarr object
        :param d:
        :param field_name:
        :param default_value:
        :return:
        """
        if 'array_options' not in d:
            return default_value
        # when extra fields are empty it is a list, not a dictionary
        if isinstance(d['array_options'], list):
            return default_value
        return d['array_options'].get('options_' + field_name, default_value)

    @staticmethod
    def set_extra_fields(definition, extra_fields):
        if extra_fields is None:
            definition['array_options'] = {}
        else:
            definition['array_options'] = {'options_' + k: extra_fields[k] for k in extra_fields.keys()}

    @staticmethod
    def set_extra_field(d, field_name, value):
        """
            set extra field value in dict representation of a Dolibarr object
        :param d:
        :param field_name:
        :param value:
        :return:
        """
        if 'array_options' not in d:
            d['array_options'] = {}
        d['array_options']['options_'+field_name] = value

    def call(self, http_call, instruction, data=None, error_if_404=False):
        """

        :param http_call:
        :param instruction: url
        :param data:
        :param error_if_404: ignore 404, returns None instead
        :return:
        """
        # url
        url = self.base_URL + instruction
        # log for debug mode
        self.logger.debug("URL : " + url)
        self.logger.debug("CALL : " + http_call)
        self.logger.debug("HEADERS : " + str(self.headers))
        if data:
            self.logger.debug("DATA/PARAMS : " + str(data))
        else:
            self.logger.debug("DATA/PARAMS : None")
        # read only
        if self.read_only and http_call != "GET":
            self.logger.info('Read only mode')
            return None
        # headers
        header = self.headers
        if http_call == "POST" or http_call == "PUT":
            header['Content-Type'] = 'application/json'
            data = json.dumps(data, default=self.json_obj_handler)
        # call
        if http_call == "GET":
            r = requests.get(url=url, headers=self.headers, params=data)
        elif http_call == "PUT":
            r = requests.put(url=url, headers=self.headers, data=data)
        elif http_call == "POST":
            r = requests.post(url=url, headers=self.headers, data=data)
        elif http_call == "DELETE":
            r = requests.delete(url=url, headers=self.headers, data=data)
        else:
            raise DolibarrRestException('HTTP call must be GET, POST, PUT or DELETE) : "%s" ' % http_call)
        # 404 as None
        if (r.status_code == 404) and (http_call == "GET") and not error_if_404:
            self.logger.debug("RESPONSE : GET 404 -> return None")
            return None
        # r.ok : REST ok
        # : test Dolibarr exception
        # : test json ok
        if r.ok:
            try:
                response = json.loads(r.text)
            except json.JSONDecodeError:
                response = r.text
        else:
            self.logger.error("r nok : " + r.text)
            raise DolibarrException(r.text)
        self.logger.debug("RESPONSE : " + r.text)

        if r.status_code != 200:  # response['error']:
            raise DolibarrRestException(response['error'])
        return response

    def test_connection(self):
        self.logger.info("Test connection to " + self.server)
        r = self.call("GET", 'status')
        self.logger.info(r)
        return r

    def map(self, _set, field):
        """
        return a list of [field, id] pairs, sorted by field
        :param _set:
        :param field:
        :return:
        """
        r = self.call("GET", _set, data={'limit': '0'})
        m = []
        for _row in r:
            m.append([_row[field], _row["id"]])
        result = sorted(m, key=lambda l: l[0])
        self.logger.info(result)
        return result

    def get(self, _url, error_if_none=False):
        """
        return
         - record if _url like xxx/ID
         - list otherwise
        :param _url:
        :param error_if_none:
        :return:
        """
        r = self.call("GET", _url, data={'limit': '0'}, error_if_404=error_if_none)
        self.logger.info(r)
        return r

    def attributes(self, _url):
        """
        return sorted list attributes names of a json representation of object or set if not empty
        :param _url:
        :return:
        """
        o = self.call("GET", _url, data={'limit': '1'})
        r = sorted(list(o[0].keys()))
        self.logger.info(r)
        return r

    def id_of(self, _url, key, value):
        """
            return if of first occurrence of value in field(key)
            todo: test unique ?
        :param _url:
        :param key:
        :param value:
        :return:
        """
        r = self.call('GET', instruction=_url, data={'sqlfilters': "t.{0} = '{1}'".format(key, value)})
        return r[0]['id']

    def get_from_key(self, _url, key, value, error_if_none=False):
        """
            return supposed unique record with field (key) = value
        :param _url:
        :param key:
        :param value:
        :param error_if_none:
        :return:
        """
        r = self.call('GET', instruction=_url, data={'sqlfilters': "t.{0} = '{1}'".format(key, value)},
                      error_if_404=error_if_none)
        if not r:
            return None
        if len(r) > 1:
            raise DolibarrRestException(
                'Get from table/key "{0}"/"{1}" returns non unique object for value {2}'.format(_url, key, value))
        return r[0]


def cli_mode():
    def parse_table_field(arg):
        result = tuple(arg.split("."))
        if len(result) < 2:
            raise Exception('Missing parameter "TABLE.FIELD" or "TABLE.ID" : "%s"' % arg)
        if len(result) > 2:
            raise Exception('Bad parameter format "TABLE.FIELD" or "TABLE.ID" : "%s"' % arg)
        return result

    def make_row_request(dol_api, header, _row):
        """
            interpret a row of csv file to call a REST api :
            column 0 : http type GET, PUT, POST ou DELETE
            column 1 : instruction or end point
            column 2 : id used for put or delete
            column 3 to xxx : data or params for GET
            for PUT or DELETE, if id is None then column 3 is used for lookup key/value

        :param dol_api: DolibarrRest object
        :param header: header of csv file
        :param _row: row of csv file
        :return: nothing, print result of get
        """
        _data = {}
        _start_of_data = 3
        # if id is None, lookup with column 4 as key
        if ((_row[0] == 'PUT') or (_row[0] == 'DELETE')) and (_row[2] == ''):
            _row[2] = dol_api.id_of(table=_row[1], key=header[3], value=_row[3])
            _start_of_data += 1
        # params/data dictionary : header(i) = cell(i)
        for _i, c in enumerate(_row[_start_of_data:]):
            if c:
                _data[header[_i + _start_of_data]] = c
        # API call
        r = dol_api.call(http_call=_row[0], instruction=_row[1]+'/'+_row[2], data=_data)
        if _row[0] == 'GET':
            print(r)

    def process_csv_file(_file):
        """
            interprets csv file as a script see make_row_request()
        :param _file:
        :return:
        """
        with open(_file, 'r') as csv_file:
            h = ''
            for i, row in enumerate(csv.reader(csv_file)):
                # headers
                if i == 0:
                    h = row
                    continue
                if len(row) == 0:
                    break
                make_row_request(dr, h, row)

    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("-c", "--conf", help="use configuration file CONF", default="./conf.yml")
    argument_parser.add_argument("-f", "--csv-file", help="process script from CSV file")
    argument_parser.add_argument("-t", "--test", help="test configuration and connection",
                                 action="store_true", default=False)
    argument_parser.add_argument("-v", "--verbose", help="verbose mode", action="store_true", default=False)
    argument_parser.add_argument("-d", "--debug", help="more verbose mode", action="store_true", default=False)
    argument_parser.add_argument("-a", "--attributes", help="list attributes of TABLE (must be not empty)")
    argument_parser.add_argument("-m", "--map", help="map TABLE.FIELD with id in stdout, csv format")
    argument_parser.add_argument("-g", "--get", help="print objects in TABLE or object TABLE/ID, etc...")

    __args = argument_parser.parse_args()

    # Configuration
    with open(__args.conf, 'r') as stream:
        yamlcfg = dict(yaml.load(stream)[0])
        __conf = yamlcfg.get("dolibarr_conf")

    if __conf is None:
        print("Configuration section not found in %s, exit" % __args.conf)
        exit(0)

    # Logging
    __logger = logging.getLogger("dolibarrAPI")
    ch = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    __logger.addHandler(ch)
    if __args.verbose:
        __logger.setLevel(logging.INFO)
    if __args.debug:
        __logger.setLevel(logging.DEBUG)

    dr = DolibarrRest(__logger, __conf)

    if __args.test:
        print(dr.test_connection())
    elif __args.attributes:
        print(dr.attributes(__args.attributes))
    elif __args.map:
        _table, _id = parse_table_field(__args.map)
        writer = csv.writer(sys.stdout, quoting=csv.QUOTE_ALL)
        writer.writerow((_id, _table + '_id'))
        writer.writerows(dr.map(_table, _id))
    elif __args.get:
        print(dr.get(__args.get))
    elif __args.csv_file:
        process_csv_file(__args.csv_file)
    # finally return instance of api
    return dr


if __name__ == '__main__':
    cli_mode()
