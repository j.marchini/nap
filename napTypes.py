"""
Created on 5 mai. 2018
@author: jerome

"""

from enum import Enum
# https://www.python.org/dev/peps/pep-0435/


class CommonObject(object):
    """
    base class of all "nap" objects
    common id is in form "id@origin"
    internal id is the proper id for the instance in it's persistant solution
    common id is shared by all nap process:
        order created in woocommerce is represented by contract in erp
        woocommerce order instance :
            internal id = woocommerce order id
            id = internal id
            common id = id@woocommerce instance
        erp contract instance :
            id = woocommerce id
            common id = woocommerce id@woocommerce instance
            internal id = erp contract id
    """
    def __init__(self, definition):
        self.internal_ID = definition['InternalID']
        self.origin = definition['Origin']
        self.ID = definition.get('ID', definition['InternalID'])
        self.date_create = definition.get("DateCreate")
        self.date_update = definition.get("DateUpdate")

    @staticmethod
    def id_of(common_id):
        """
          internal id part of common id: internal_id@origin
        :param common_id:
        :return: internal id
        """
        return common_id.partition('@')[0]

    @staticmethod
    def origin_of(common_id):
        """
            origin part of common id : internal_id@origin
        :param common_id:
        :return: origin
        """
        return common_id.partition('@')[2]

    @staticmethod
    def common_id_of(_id, origin):
        """return common id form : "id@origin" """
        return str(_id) + '@' + origin

    def common_id(self):
        """return common id of instance id@origin"""
        return self.common_id_of(self.ID, self.origin)

    def update_with(self, common_object):
        """copy values from common_object"""
        pass


# thirdparties
class CommonCustomer(CommonObject):
    """ base customer class"""
    def __init__(self, definition):
        super().__init__(definition)
        self.name = definition["Name"]
        self.first_name = definition["FirstName"]
        self.address_list = definition["AddressList"]
        self.zip = definition["Zip"]
        self.town = definition["Town"]
        self.e_mail = definition["EMail"]
        self.phone = definition["Phone"]

    def update_with(self, common_client):
        """customer copy method"""
        CommonObject.update_with(self, common_client)
        self.name = common_client.name
        self.first_name = common_client.first_name
        self.address_list = common_client.address_list
        self.zip = common_client.zip
        self.town = common_client.town
        self.e_mail = common_client.e_mail
        self.phone = common_client.phone


# products
class CommonProduct(CommonObject):
    """base product class"""
    def __init__(self, definition):
        super().__init__(definition)
        self.key = definition['Key']
        self.name = definition['Name']


# orders
class CommonOrder(CommonObject):
    """base order class"""
    def __init__(self, definition):
        super().__init__(definition)
        self.customer_ID = definition['Customer_ID']
        self.date_completed = definition['Date_completed']
        self.status = definition['Status']
        self.lines = definition['Lines']
        self.customer = definition['Customer']


# invoices
class CommonCustomerInvoice(CommonObject):
    """base customer invoice class"""
    def __init__(self, definition):
        super().__init__(definition)
        self.client = None
        self.customer_ref = definition['Customer_ref']
        self.description = definition['Description']
        self.invoice_date = definition['Invoice_date']
        self.proposal_ref = definition['Proposal_ref']
        self.reference = definition['Reference']
        self.status = definition['Status']
        self.lines = []
        self.invoice_line_class = CommonCustomerInvoiceLine

    def update_with(self, common_invoice):
        """customer invoice copy"""
        CommonObject.update_with(self, common_invoice)
        self.reference = common_invoice.reference
        self.client = common_invoice.client
        self.customer_ref = common_invoice.customer_ref
        self.description = common_invoice.description
        self.proposal_ref = common_invoice.proposal_ref
        self.invoice_date = common_invoice.invoice_date
        self.status = common_invoice.status
        self.lines = common_invoice.lines
        self.invoice_line_class = common_invoice.invoice_line_class


class CommonCustomerInvoiceLine(CommonObject):
    """ base customer invoice line"""
    def __init__(self, definition):
        super().__init__(definition)
        self.designation = definition['Designation']
        self.product_type = definition['Product_type']
        self.quantity = definition['Quantity']
        self.subprice = definition['Subprice']
        self.tax_rate = definition['Tax_rate']


class CommonCustomerInvoiceStatus(Enum):
    """ todo: check if really common"""
    draft = 0
    validated = 1
    paid = 2


# contracts
class CommonContract(CommonObject):
    """base common contract class"""
    def __init__(self, definition):
        super().__init__(definition)
        self.client = None
        self.ref = definition['Ref']
        self.status = definition['Status']
        self.date_contract = definition['Date_contract']
        self.services = []
        self.service_class = CommonService


class CommonContractStatus(Enum):
    """ todo: check if really common"""
    draft = 1
    validated = 2
    closed = 3


# services - contract lines
class CommonServiceStatus(Enum):
    """todo: check if really common"""
    initial = 1
    running = 2
    closed = 3


class CommonService(CommonObject):
    """base service class"""
    def __init__(self, definition):
        super().__init__(definition)
        self.status = definition['Status']
        self.quantity = definition['Quantity']
        self.ref = definition['Ref']
        self.product_ref = definition['Product_ref']
        self.date_start = definition['Date_start']
        self.date_end = definition['Date_end']
