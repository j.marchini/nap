#!/usr/bin/python3

"""
Created on 2 mai. 2018

@author: jerome

composing, drafting, sending mails for human beings
"""

import smtplib
from email.utils import formatdate, formataddr
from email.encoders import encode_base64
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from imaplib import IMAP4
# imap for human beings
import imapclient
import ssl


class ComposeMailException(Exception):
    """empty exception"""
    pass


def compose_email(subject, body, to=None, from_addr="", cc=None, bcc=None, files_to_attach=None, html=False):
    """
    mail composition in pythonic way, for human beings
    :param subject:
    :param body:
    :param to: recipient or list of recipients
    :param from_addr:
    :param cc: recipient or list of recipients
    :param bcc: recipient or list of recipients
    :param files_to_attach: list of file to attach
    :param html: boolean HTML or plain
    :return:
    """

    if (to == []) and (cc == []) and (bcc == []):
        raise ComposeMailException("No recipient, aborting")

    if html:
        subtype = 'html'
    else:
        subtype = 'plain'
    # create the message
    if files_to_attach:
        _msg = MIMEMultipart()
        _msg.attach(MIMEText(body, subtype, _charset='utf8'))
    else:
        _msg = MIMEText(body, subtype, _charset='utf8')
    # _msg.set_unixfrom('author')
    _msg['From'] = formataddr((False, from_addr))
    _msg["Subject"] = subject
    _msg["Date"] = formatdate(localtime=True)

    if to:
        if isinstance(to, str):
            _msg["To"] = to
        else:
            _msg["To"] = ', '.join(to)
    if cc:
        if isinstance(cc, str):
            _msg["Cc"] = cc
        else:
            _msg["Cc"] = ', '.join(cc)
    if bcc:
        if isinstance(bcc, str):
            _msg["Bcc"] = bcc
        else:
            _msg["Bcc"] = ', '.join(bcc)

    if files_to_attach:
        for file_to_attach in files_to_attach:
            attachment = MIMEBase('application', "octet-stream")
            try:
                with open(file_to_attach, "rb") as fh:
                    data = fh.read()
                attachment.set_payload(data)
                encode_base64(attachment)
                header = 'Content-Disposition', 'attachment; filename="%s"' % file_to_attach
                attachment.add_header(*header)
                _msg.attach(attachment)
            except IOError:
                raise ComposeMailException("Error opening attachment file %s" % file_to_attach)
    return _msg


def draft_email(email, host, user, password):
    """
    put a mail in draft box of IMAP account
    :param email:
    :param host:
    :param user:
    :param password:
    :return: nothing
    """
    try:
        server = imapclient.IMAPClient(host=host, ssl=True)
    except IMAP4.error:
        ssl_context = ssl.create_default_context()
        server = imapclient.IMAPClient(host=host, ssl_context=ssl_context, ssl=True)
    server.login(user, password)
    if email:
        server.append('Drafts', str(email))
    server.logout()


def send_email(email, host, port, user, password):
    """
    send an email previously composed
    :param email:
    :param host:
    :param port:
    :param user:
    :param password:
    :return:  nothing
    """
    # connection to the server
    server = smtplib.SMTP(host=host, port=port)
    try:
        server.ehlo()
        # If we can encrypt this session, do it
        if server.has_extn('STARTTLS'):
            server.starttls()
            server.ehlo()  # re-identify ourselves over TLS connection
        server.login(user, password)
        # sending the message
        server.send_message(email)
    finally:
        server.quit()


def test_connection(host, port, user, password):
    """testing connection to the server"""
    server = smtplib.SMTP(host=host, port=port)
    try:
        server.ehlo()
        # If we can encrypt this session, do it
        if server.has_extn('STARTTLS'):
            server.starttls()
            server.ehlo()  # re-identify ourselves over TLS connection
        server.login(user, password)
    finally:
        server.quit()
