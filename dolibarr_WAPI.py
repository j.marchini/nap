#!/usr/bin/env python3

"""
Created on 07 oct. 2018
@author: jerome
Dolibarr Workaround API
to be used until official API bugs will be fixed
"""

from datetime import datetime, date
import json
import requests

import sys
import yaml
from napUtils import DefaultLogger


class DolibarrWAPIException(Exception):
    """empty exception"""
    pass


class DolibarrWAPI:
    """ Dolibarr workaround API """
    def __init__(self):
        self.base_URL = ""
        self.headers = {}
        self.logger = None

    def initialize(self, logger, conf):
        """ initialize with configuration"""
        self.base_URL = conf['base_url']
        self.headers = {'Api-Key': conf['api_key'], }
        self.logger = logger

    # json facilities
    @staticmethod
    def _json_obj_handler(obj):
        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        # # enum is serialized as string, not integer, to be more readable
        # if isinstance(obj, Enum):
        #     return obj.name
        raise DolibarrWAPIException('Class "%s" not defined for json serialization' % obj.__name__)

    def _call(self, http_call, instruction, data=None):
            # url
            url = self.base_URL + instruction
            # log for debug mode
            self.logger.debug("URL : " + url)
            self.logger.debug("CALL : " + http_call)
            self.logger.debug("HEADERS : " + str(self.headers))
            if data:
                self.logger.debug("DATA/PARAMS : " + str(data))
            else:
                self.logger.debug("DATA/PARAMS : None")
            # headers
            header = self.headers
            http_call = http_call.upper()
            if http_call == "POST" or http_call == "PUT":
                header['Content-Type'] = 'application/json'
                data = json.dumps(data, default=self._json_obj_handler)
            # call
            if http_call == "GET":
                r = requests.get(url=url, headers=self.headers, params=data)
            elif http_call == "PUT":
                r = requests.put(url=url, headers=self.headers, data=data)
            elif http_call == "POST":
                r = requests.post(url=url, headers=self.headers, data=data)
            elif http_call == "DELETE":
                r = requests.delete(url=url, headers=self.headers, data=data)
            else:
                raise DolibarrWAPIException('HTTP call must be GET, POST, PUT or DELETE) : "%s" ' % http_call)
            if r.ok:
                try:
                    response = json.loads(r.text)
                except json.JSONDecodeError:
                    response = r.text
            else:
                self.logger.error("r nok : " + r.text)
                raise DolibarrWAPIException(r.text)
            self.logger.debug("RESPONSE : " + r.text)

            if r.status_code != 200:  # response['error']:
                raise DolibarrWAPIException(response['error'])
            return response

    def update_invoice_customer_ref(self, invoice_id, customer_ref):
        """
        updates ref_client field in dolibarr and set tms field to current timestamp
        :param invoice_id:
        :param customer_ref:
        :return:
        """
        return self._call('get', 'facture/update_ref_client', {'id': invoice_id, 'ref_value': customer_ref})

    def touch_thirdparty(self, thirdparty_id):
        """
        touch the thirdparty: set tms field to current timestamp
        :param thirdparty_id:
        :return:
        """
        return self._call('get', f'thirdparty/touch/{thirdparty_id}')


if __name__ == '__main__':
    # Configuration
    with open('conf.yml', 'r') as stream:
        yamlcfg = dict(yaml.load(stream)[0])
        api = DolibarrWAPI()
        api.initialize(DefaultLogger(), yamlcfg.get("dolibarr_WAPI_conf"))
        if len(sys.argv) > 1:
            print(api._call('get', sys.argv[1], {'bar': 5}))
        else:
            print(api._call('get', ''))
