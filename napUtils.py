#! /usr/bin/env python3
"""
nap utilities
"""


import random
from datetime import timedelta, date
import dateutil.parser


def iso_to_datetime(d):
    """convert iso 8601 datetime to python datetime"""
    return dateutil.parser.parse(d)


def date_past(_date, _days=0):
    """
    Determines if a date is past, with a difference in option
    date_past(date) : date past
    date_past(date, 5) : date past 5 days ago
    date_past(date, -10) : less than 10 days between today and date
    :param _date:
    :param _days:
    :return: Boolean
    """
    return date.today() >= _date + timedelta(days=_days)


def password(long):
    """
    make a complicated enough password
    :param long: length of the password
    :return: the complicated enough password in question
    """
    def drop_char(string, char):
        """
            drop a char somewhere in a string
        """
        if string == "":
            return char
        pos = random.randint(0, len(string) - 1)
        return string[:pos] + char + string[pos:]

    if long < 4:
        raise Exception("Password must be at least 4 characters long to be complicated enough")
    element1 = "+-*/~$%&.:?!"
    element2 = "abcdefghijklmnopqrstuvwxyz"
    element3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    element4 = "0123456789"
    element = element1 + element2 + element3 + element4

    p = ""
    for i in range(long-4):
        p = drop_char(p, random.choice(element))
    p = drop_char(p, random.choice(element1))
    p = drop_char(p, random.choice(element2))
    p = drop_char(p, random.choice(element3))
    p = drop_char(p, random.choice(element4))
    return p


class DefaultLogger:
    """
    provides a default logger for debug or test purpose
    """
    level_info = 10
    level_warning = 20
    level_error = 30
    level_debug = 40

    def __init__(self, level=level_error):
        self.level = level

    def info(self, msg):
        """print moke"""
        if self.level >= self.level_info:
            print(msg)

    def debug(self, msg):
        """print moke"""
        if self.level >= self.level_debug:
            print(msg)

    def error(self, msg):
        """print moke"""
        if self.level >= self.level_error:
            print(msg)
