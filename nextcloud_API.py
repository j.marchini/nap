#!/usr/bin/python3
"""
Created on 29 mai. 2018
@author: jerome
Nextcloud 12, 13
api V1
"""

import json
import requests
import napUtils
import re


class NextcloudAPI:
    """
    Nextcloud pythonic API
    """
    class NextcloudApiException(Exception):
        """
        empty exception
        """
        pass

    server_token = "server"
    user_token = "user"
    password_token = "password"

    client_group = "CLIENT"
    collabora_group = "COLLABORA"
    default_quota = "3GB"

    sequence_base = "CustID_"

    collabora_option = "collabora"

    def __init__(self, logger, conf):
        self.base_URL = "https://{0}/"
        self.headers = {'OCS-APIRequest': 'true', 'Content-Type': 'application/x-www-form-urlencoded'}
        self.server = conf[self.server_token]
        self.logger = logger
        self.user = conf[self.user_token]
        self.password = conf[self.password_token]
        self.base_URL = self.base_URL.format(self.server) + "ocs/v1.php/cloud/"
        self.read_only = conf.get('read_only')

    def _rest_call(self, http_call, instruction, params=None):
        url = self.base_URL + instruction
        self.logger.debug("URL : " + url)
        self.logger.debug("INSTRUCTION : " + instruction)
        if params:
            self.logger.debug("DATA/PARAMS : " + str(params))
        else:
            self.logger.debug("DATA/PARAMS : None")
        self.logger.debug("HEADERS : " + str(self.headers))

        if self.read_only and http_call != "GET":
            self.logger.info('Read only mode')
            return None

        if params is not None:
            params['format'] = 'json'
        else:
            params = {'format': 'json'}

        if http_call == "GET":
            r = requests.get(url=url, headers=self.headers, params=params, auth=(self.user, self.password))
        elif http_call == "PUT":
            r = requests.put(url=url, headers=self.headers, data=params, auth=(self.user, self.password))
        elif http_call == "POST":
            r = requests.post(url=url, headers=self.headers, data=params, auth=(self.user, self.password))
        elif http_call == "DELETE":
            r = requests.delete(url=url, headers=self.headers, data=params, auth=(self.user, self.password))
        else:
            raise Exception("HTTP call must be in (GET POST PUT DELETE) : %s " % http_call)

        response = json.loads(r.text)
        if r.status_code != 200 or response['ocs']['meta']['statuscode'] != 100:
            raise self.NextcloudApiException(r)
        return response

    def test_connection(self):
        """
        test connection and exit
        :return:
        """
        self.logger.info("Test connection to " + self.server)
        self._rest_call("GET", 'users', params={"search": self.user})
        self.logger.info("Connection OK " + self.server)
        return True

    def _add_user(self, user_id, password):
        """
        add user
        :param user_id:
        :param password:
        :return:
        """
        self.logger.info('new user "{0}" identified by "{1}"'.format(user_id, password))
        self._rest_call("POST", 'users', params={"userid": user_id, "password": password})

    def _add_user_group(self, user_id, group_id):
        """
        add user in group
        :param user_id:
        :param group_id:
        :return:
        """
        self.logger.info('add user "{0}" in group "{1}"'.format(user_id, group_id))
        lg = self.list_groups()
        if group_id not in lg:
            self.add_group(group_id)
        self._rest_call("POST", 'users/'+user_id+'/groups', params={"groupid": group_id})

    def _delete_user(self, user_id):
        self.logger.info('delete user "{0}"'.format(user_id))
        self._rest_call("DELETE", 'users/'+user_id)

    def _get_user(self, user_id):
        return self._rest_call("GET", 'users/'+user_id)['ocs']['data']

    def list_users(self):
        """
        list all users on the instance
        :return:
        """
        return self._rest_call("GET", 'users')['ocs']['data']['users']

    def _edit_user(self, user_id, key, value):
        """edit user data, keys available: email quota displayname phone address website twitter password"""
        self.logger.info('edit user "{0}" : key = "{1}" value = "{2}"'.format(user_id, key, value))
        self._rest_call("PUT", 'users/'+user_id, {'key': key, 'value': value})

    def _new_user_id(self):
        """sequence based on sequence_base + max ID"""
        lu = self.list_users()
        max_id = 0
        r = re.compile('^{0}(?P<id>.*)'.format(self.sequence_base))
        for u in lu:
            m = r.match(u)
            if m:
                lid = int(m.groups()[0])
                if max_id < lid:
                    max_id = lid
        return '{0}{1}'.format(self.sequence_base, max_id+1)

    def _new_account(self, user_id, display_name, email, collabora):
        """
        create a user with data, generate a password using napUtils.password
        :param user_id:
        :param display_name:
        :param email:
        :param collabora:
        :return: user_id, pwd
        """
        pwd = napUtils.password(13)
        self._add_user(user_id, pwd)
        self._edit_user(user_id, "displayname", display_name)
        self._edit_user(user_id, "quota", self.default_quota)
        self._edit_user(user_id, "email", email)
        self._add_user_group(user_id, self.client_group)
        if collabora:
            self._add_user_group(user_id, self.collabora_group)
        return user_id, pwd

    def new_client_account(self, display_name, email, options=None):
        """
        new client account
        :param display_name:
        :param email:
        :param options:
        :return:
        """
        c = options and options.get(self.collabora_option)
        user_id = self._new_user_id()
        return self._new_account(user_id, display_name, email, c)

    def disable_account(self, user_id):
        """disable account"""
        self.logger.info('disable user "{0}"'.format(user_id))
        self._rest_call("PUT", 'users/'+user_id+'/disable')

    def delete_account(self, user_id):
        """ delete account"""
        self.logger.info('delete user "{0}"'.format(user_id))
        self._rest_call("DELETE", 'users/'+user_id)

    def list_groups(self, search=None):
        """list all groups on instance"""
        if search:
            return self._rest_call("GET", 'groups', params={'search': search})['ocs']['data']['groups']
        else:
            return self._rest_call("GET", 'groups')['ocs']['data']['groups']

    def add_group(self, group_id):
        """create a group"""
        self.logger.info('add group "{0}"'.format(group_id))
        self._rest_call("POST", 'groups', params={"group_id": group_id})


if __name__ == '__main__':

    import yaml
    import logging
    import argparse

    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("-c", "--conf", help="use configuration file CONF", default="./conf.yml")
    argument_parser.add_argument("-t", "--test", help="test configuration and connection",
                                 action="store_true", default=False)
    argument_parser.add_argument("-v", "--verbose", help="verbose mode", action="store_true", default=False)
    argument_parser.add_argument("-d", "--debug", help="more verbose mode", action="store_true", default=False)
    argument_parser.add_argument("-l", "--list-users", help="list users", action="store_true", default=False)

    __args = argument_parser.parse_args()

    with open(__args.conf, 'r') as stream:
        yamlcfg = dict(yaml.load(stream)[0])
        __conf = yamlcfg.get("nextcloud_conf")

    __logger = logging.getLogger("nextcloudAPI")
    ch = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    __logger.addHandler(ch)
    if __args.verbose:
        __logger.setLevel(logging.INFO)
    if __args.debug:
        __logger.setLevel(logging.DEBUG)

    nextcloud_rest = NextcloudAPI(__logger, __conf)

    if __args.test:
        nextcloud_rest.test_connection()
    elif __args.list_users:
        print(nextcloud_rest.list_users())
    else:
        # here to code for debug
        pass
