#!/usr/bin/python3
"""
Created on 2 mai. 2018
@author: jerome

"""
import argparse
import os

from napProcessor import NapFactory

if __name__ == '__main__':
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("-c", "--conf", help="use configuration file CONF", default="./conf.yml")
    argument_parser.add_argument("-t", "--test", help="test configuration and connection",
                                 action="store_true", default=False)
    argument_parser.add_argument("-v", "--verbose", help="verbose mode", action="store_true", default=False)
    argument_parser.add_argument("-d", "--debug", help="more verbose mode", action="store_true", default=False)

    args = argument_parser.parse_args()

    if not os.path.exists(args.conf):
        print('Configuration file "%s" not found' % args.conf)
        exit(1)

    NapFactory(args)
