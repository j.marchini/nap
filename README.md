NAP -  network application processor

- is a simple framework 
- is aimed to do application integration with no effort 
- is written in python 3
- is written by a human being for human beings
- is intended to run on a server with an external scheduler 

- interfaces with rest api :
    - Dolibarr
    - Woocommerce
    - Nextcloud
    - ...

- interfaces with mail :
    - SMTP
    - IMAP (draft mails)

- uses logging module and SMTP to report alerts and activity

- uses jinja2 for templates

- uses a single configuration file in YAML (see conf.yml.example)



