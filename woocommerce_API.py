#!/usr/bin/python3
"""
Created on 2 mai. 2018
@author: jerome
https://docs.woocommerce.com/document/woocommerce-rest-api/
"""

from woocommerce import API, __version__
from requests import request

import napTypes
from dateutil import parser

# configuration and connection
server_token = "woocommerce_server"
consumer_key_token = "consumer_key"
consumer_secret_token = "consumer_secret"
query_string_auth_token = "query_string_auth"
version_API_token = "version_API"
timeout_token = "timeout"

product_key_attribute = 'Product_Key_NAP'
default_timeout = 30


class WoocommerceApi:
    def __init__(self, logger, conf):
        self.server = ""
        self.headers = {}
        self.logger = logger

        self.server = conf[server_token]
        self.url = "https://{0}".format(self.server)
        self.consumer_key = conf[consumer_key_token]
        self.consumer_secret = conf[consumer_secret_token]
        self.query_string_auth = conf.get(query_string_auth_token, False)
        self.version_API = conf.get(version_API_token, "v2")
        self.timeout = conf.get(timeout_token, default_timeout)

        self.read_only = conf.get('read_only')
        self.wcapi = None

    def get(self, endpoint, params=None):
        """
        full get, loop until no more record, beware of cursor stability !
        :param endpoint:
        :param params:
        :return:
        """
        if params is None:
            params = {"per_page": 10, "page": 1}
        else:
            params["per_page"] = 10
            params["page"] = 1

        items = self._getp(endpoint, params).json()[endpoint]
        while len(items) > 0:
            for item in items:
                yield item
            params["page"] += 1
            items = self._getp(endpoint, params).json()[endpoint]

    def _getp(self, endpoint, params):
        def get_url(api_url, version, wp_api, end_point):
            """ Get URL for requests """
            api = "wc-api"
            if api_url.endswith("/") is False:
                api_url = "%s/" % api_url
            if wp_api:
                api = "wp-json"
            return "%s%s/%s/%s" % (api_url, api, version, end_point)

        url = get_url(self.wcapi.url, self.wcapi.version, self.wcapi.wp_api, endpoint)
        auth = None
        headers = {
            "user-agent": "WooCommerce API Client-Python/%s" % __version__,
            "accept": "application/json"
        }

        if self.wcapi.is_ssl is True and self.wcapi.query_string_auth is False:
            auth = (self.wcapi.consumer_key, self.wcapi.consumer_secret)
        elif self.wcapi.is_ssl is True and self.wcapi.query_string_auth is True:
            if params is None:
                params = {
                    "consumer_key": self.wcapi.consumer_key,
                    "consumer_secret": self.wcapi.consumer_secret
                }
            else:
                params["consumer_key"] = self.wcapi.consumer_key
                params["consumer_secret"] = self.wcapi.consumer_secret
        else:
            url = self.wcapi.__get_oauth_url(url, "GET")

        return request(
            method="GET",
            url=url,
            verify=self.wcapi.verify_ssl,
            auth=auth,
            params=params,
            data=None,
            timeout=self.wcapi.timeout,
            headers=headers
        )

    def initialize(self):
        self.wcapi = API(
            url=self.url,
            consumer_key=self.consumer_key,
            consumer_secret=self.consumer_secret,
            version=self.version_API,
            query_string_auth=self.query_string_auth,
            timeout=self.timeout)

    def test_connection(self):
        self.logger.info("Test connection to " + self.server)
        api = API(
            url=self.url,
            consumer_key=self.consumer_key,
            consumer_secret=self.consumer_secret,
            version=self.version_API,
            query_string_auth=self.query_string_auth,
            timeout=self.timeout)

        entry = "customers"
        r = api.get(entry)
        if r.status_code == 200:
            self.logger.info("Connection OK at " + self.server)
        else:
            self.logger.info('Connection error %s at %s / %s : "%s"' % (r.status_code, self.server, entry, r.text))
            self.logger.info('consumer key : "%s" ' % self.consumer_key)
            self.logger.info('consumer secret : "%s" ' % self.consumer_secret)

    def list_clients(self):
        for c in self.get("customers"):
            yield napTypes.CommonCustomer({
                "InternalID": c['id'],
                "ID": c['id'],
                "Origin": self.server,
                "Name": c['last_name'],
                "FirstName": c['first_name'],
                "AddressList": [c['billing_address']['address_1'], c['billing_address']['address_2']],
                "Zip": c['billing_address']["postcode"],
                "Town": c['billing_address']["city"],
                "EMail": c["email"],
                "Phone": c['billing_address']["phone"],
                "DateCreate":
                    parser.parse(c["created_at"]),
                "DateUpdate":
                    parser.parse(c["last_update"]),
                })

    def list_products(self):
        for p in self.get("products"):
            if p.get("status") != "publish":
                continue
            title = p.get("title")
            k = None
            for a in p.get("attributes"):
                if a["name"] == product_key_attribute:
                    o = a['options']
                    if len(o) != 1:
                        self.logger.error('Woocommerce product "{0}" has no unique key attribute'.format(title))
                        continue
                    k = o[0]
            yield napTypes.CommonProduct({
                'InternalID': p['id'],
                'ID': p['id'],
                'Origin': self.server,
                'DateUpdate':
                    parser.parse(p["updated_at"]),
                'Key': k,
                'Name': title,
            })

    def list_orders(self):
        for o in self.get("orders"):
            if o['status'] == 'completed':
                if o['customer_id'] == 0:
                    self.logger.error('Woocommerce order "{0}" has no customer id'.format(o['id']))
                    continue
                yield napTypes.CommonOrder({
                    'InternalID': o['id'],
                    'ID': o['id'],
                    'Origin': self.server,
                    'DateUpdate':
                        parser.parse(o["updated_at"]),
                    'Date_completed':
                        parser.parse(o["completed_at"]),
                    'Customer_ID': o['customer_id'],
                    'Customer': o['customer'],
                    'Status': o['status'],
                    'Lines': [{'product_id': l['product_id'], 'quantity': l['quantity']} for l in o['line_items']],
                })


if __name__ == '__main__':
    import yaml
    import napUtils
    with open("./conf.yml", 'r') as stream:
        yamlcfg = dict(yaml.load(stream)[0])
    WoocommerceApi(napUtils.DefaultLogger(), yamlcfg.get("woo_commerce_conf")).test_connection()
