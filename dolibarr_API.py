#!/usr/bin/python3
"""
Created on 2 mai. 2018
@author: jerome
Dolibarr V7.02, V7.03
"""

import dolibarr_REST
from datetime import datetime, date, timedelta
import time
import copy
from dateutil import tz

from napTypes import *


# connection settings
dol_api_key_token = "dolibarr_api_key"
server_token = "dolibarr_server"

# server settings
default_timezone = tz.gettz('Europe/Paris')

# common object settings
extra_fields_token = 'extra_fields'

# thirdparties
default_thirdparty_key = 'idprof6'

# contracts
default_contract_key = 'ref_ext'
default_commercial_id = 1


class DolibarrApiException(Exception):
    """empty exception"""
    pass


class ExtraFields(object):
    """
    extra fields facility
    TODO: doc
    """
    def __init__(self, definition, keys):
        # set of extra fields names
        self._fields = set()
        for k in keys:
            self.set_extra_field_from_def(definition, k)

    def set_extra_field_from_def(self, definition, key, default_value=None):
        """
        references extra fields in private _fields attributes
        and add it to instance as normal attribute"""
        self._fields.add(key)
        # dolibarr REST API is not normalized we always have to check everything
        if definition.get(extra_fields_token) is None:
            self.__setattr__(key, default_value)
        else:
            self.__setattr__(key, definition[extra_fields_token].get(key, default_value))

    def get_extra_fields(self):
        """
        access to all extra fields of instance
        :return:
        """
        return {key: self.__getattribute__(key) for key in self._fields}


class DolibarrApi:
    """
    Dolibarr pythonic access to API
    """
    def __init__(self):
        self.logger = None
        self.server = ""
        self.read_only = False
        self.timezone = default_timezone
        self.thirdparty_key = default_thirdparty_key
        self.contract_key = default_contract_key
        self.commercial_id = default_commercial_id
        self.product_cache = {}
        self.thirdparty_cache = {}
        self._rest = None

    def initialize(self, logger, conf):
        """apply configuration"""
        self.logger = logger
        self.server = conf[server_token]
        self.read_only = conf.get('read_only', False)
        self.timezone = conf.get('time_zone', default_timezone)
        self.thirdparty_key = conf.get('thirdparty_key', default_thirdparty_key)
        self.contract_key = conf.get('contract_key', default_contract_key)
        self.commercial_id = conf.get('commercial_id', default_commercial_id)
        self.product_cache = {}
        self.thirdparty_cache = {}
        self._rest = dolibarr_REST.DolibarrRest(self.logger, conf)

    def test_connection(self):
        """test connection and exit"""
        self._rest.test_connection()

    # clients
    def _dol_client(self, common_client):
        """
        build values for POST or PUT calls
        :param common_client:
        :return:
        """
        result = {
            'name': common_client.name + ' ' + common_client.first_name,
            self.thirdparty_key: common_client.common_id(),
            'email': common_client.e_mail,
            'phone': common_client.phone,
            'address': '\n'.join(common_client.address_list),
            'zip': common_client.zip,
            'town': common_client.town,
            'fournisseur': 0,
            'client': 1, }
        if isinstance(common_client, ExtraFields):
            self._rest.set_extra_fields(result, common_client.get_extra_fields())
        return result

    def _thirdparty_object(self, thirdparty, thirdparty_class=CommonCustomer):
        """
        returns thirdparty object from Dolibarr values
        :param thirdparty:
        :param thirdparty_class:
        :return:
        """
        # if not initially created by Dolibarr then third_party_key field contains common id
        d = {}
        if thirdparty[self.thirdparty_key] != "":
            d['ID'] = CommonCustomer.id_of(thirdparty[self.thirdparty_key])
            d['Origin'] = CommonCustomer.origin_of(thirdparty[self.thirdparty_key])
        else:
            d['ID'] = thirdparty['id']
            d['Origin'] = self.server
        d['Name'] = thirdparty['name']
        d["FirstName"] = ""
        d["AddressList"] = thirdparty['address'].split()
        d["Zip"] = thirdparty['zip']
        d["Town"] = thirdparty['town']
        d["EMail"] = thirdparty['email']
        d["Phone"] = thirdparty['phone']
        d['InternalID'] = thirdparty['id']
        d['DateCreate'] = self._rest.timestamp_to_datetime(thirdparty['date_creation'])
        d['DateUpdate'] = self._rest.timestamp_to_datetime(thirdparty['date_modification'])
        d[extra_fields_token] = self._rest.get_extra_fields(thirdparty)
        return thirdparty_class(d)

    def add_client(self, common_client):
        """
        store a new client into Dolibarr
        :param common_client:
        :return:
        """
        try:
            self.logger.info("New client {0}".format(common_client.common_id()))
            return self._rest.call('POST', 'thirdparties', data=self._dol_client(common_client))
        except dolibarr_REST.DolibarrException as e:
            raise DolibarrApiException(e)

    def get_client_from_common_id(self, common_id, client_class=CommonCustomer):
        """
        return object from Dolibarr thirdparties by ID
        :param common_id:
        :param client_class:
        :return:
        """
        d = self._rest.get_from_key('thirdparties', self.thirdparty_key, common_id)
        if d:
            return self._thirdparty_object(d, client_class)
        else:
            return None

    def thirdparty(self, _id):
        """
        return a thirdparty set of values from Dolibarr using a cache
        :param _id:
        :return:
        """
        c = self.thirdparty_cache.get(_id)
        if not c:
            c = self._rest.call('GET', 'thirdparties/'+_id)
            self.thirdparty_cache[_id] = c
        return c

    def update_client(self, dolibarr_client, common_client=None):
        """
        updates a client into Dolibarr
        :param dolibarr_client:
        :param common_client:
        :return:
        """
        if common_client is not None:
            dolibarr_client.update_with(common_client)
        self.logger.info("Update client {0}".format(dolibarr_client.common_id()))
        return self._rest.call('PUT', 'thirdparties/'+dolibarr_client.internal_ID,
                               data=self._dol_client(dolibarr_client))

    # invoices
    def _customer_invoice_object(self, customer_invoice, customer_invoice_class=CommonCustomerInvoice):
        """
        return a invoice object from Dolibarr values
        :param customer_invoice:
        :param customer_invoice_class:
        :return:
        """
        d = {
            'InternalID': customer_invoice['id'],
            'ID': customer_invoice['id'],
            'Origin': self.server,
            'Reference': customer_invoice['ref'],
            'Customer_ref': customer_invoice['ref_client'],
            'Description': customer_invoice['note_public'],
            'Invoice_date': self._rest.timestamp_to_date(customer_invoice['date']),
            'Status': customer_invoice['statut'],
            'Proposal_ref': 'to be implemented',
            'DateCreate': self._rest.timestamp_to_datetime(customer_invoice['date_creation']),
        }
        if customer_invoice.get('date_modification'):
            d['DateUpdate'] = self._rest.timestamp_to_datetime(customer_invoice['date_modification'])
        else:
            d['DateUpdate'] = self._rest.timestamp_to_datetime(customer_invoice['date_creation'])
        d[extra_fields_token] = self._rest.get_extra_fields(customer_invoice)
        i = customer_invoice_class(d)
        # i.client = self._thirdparty_object(self.thirdparty(contract['fk_soc']))
        i.client = self._thirdparty_object(self.thirdparty(customer_invoice['socid']))
        for l in customer_invoice['lines']:
            i.lines.append(self._customer_invoice_line_object(l, i.invoice_line_class))
        return i

    def _customer_invoice_line_object(self, invoice_line, invoice_line_class=CommonCustomerInvoiceLine):
        d = dict()
        d['InternalID'] = invoice_line['id']
        d['Origin'] = self.server
        d['Designation'] = invoice_line['libelle']
        d['Product_type'] = 'Product_type'
        d['Quantity'] = invoice_line['qty']
        d['Subprice'] = invoice_line['subprice']
        d['Tax_rate'] = invoice_line['tva_tx']
        d[extra_fields_token] = self._rest.get_extra_fields(invoice_line)
        return invoice_line_class(d)

    def _dol_customer_invoice(self, common_invoice):
        """
        build values for POST or PUT calls
        :param common_invoice:
        :return:
        """
        result = {
            # 'ref': common_invoice.reference,
            # 'ref_client': common_invoice.customer_ref,
            # 'note_public': common_invoice.description,
            # 'date': self._rest.datetime_to_dol_timestamp(common_invoice.invoice_date),
            # not fair to update this kind of data...
            # to be implemented or not
            # 'statut': ...
            }
        # for l in customer_invoice['lines']:
        if isinstance(common_invoice, ExtraFields):
            self._rest.set_extra_fields(result, common_invoice.get_extra_fields())
        return result

    def all_invoices(self, invoice_class):
        """return all invoices as object"""
        di = self._rest.call('GET', 'invoices')
        if di is None:
            return None
        for i in di:
            yield self._customer_invoice_object(i, invoice_class)

    def update_invoice(self, dolibarr_invoice, common_invoice=None):
        """
        updates an invoice into Dolibarr
        :param dolibarr_invoice:
        :param common_invoice:
        :return:
        """
        if common_invoice is not None:
            dolibarr_invoice.update_with(common_invoice)
        self.logger.info("Update invoice {0}".format(dolibarr_invoice.common_id()))
        return self._rest.call('PUT', 'invoices/'+dolibarr_invoice.internal_ID,
                               data=self._dol_customer_invoice(dolibarr_invoice))

    # products
    def _product_object(self, product, product_class=CommonProduct):
        """
        return a Product object from Dolibarr values
        :param product:
        :param product_class:
        :return:
        """
        d = {
            'Key': product['ref'],
            'Name': product['label'],
            'InternalID': product['id'],
            'ID': product['id'],
            'Origin': self.server,
            'DateUpdate': None,
        }
        if product.get('date_modification'):
            d['DateUpdate'] = self._rest.dolibarr_datetime_to_datetime(product['date_modification'])
        else:
            d['DateUpdate'] = self._rest.dolibarr_datetime_to_datetime(product['date_creation'])
        d[extra_fields_token] = self._rest.get_extra_fields(product)
        return product_class(d)

    def product_by_ref(self, reference, product_class=CommonProduct):
        """return product object matching ref"""
        dp = self._rest.get_from_key('products', 'ref', reference)
        if dp:
            return self._product_object(dp, product_class)
        else:
            return None

    # contracts
    def _dol_new_contract(self, common_order, client_map):
        return {
            'socid': client_map[common_order.customer_ID],
            self.contract_key: common_order.common_id(),
            'ref': common_order.common_id(),
            'date_contrat': common_order.date_completed,
            'date_start': common_order.date_completed,
            'date_end': common_order.date_completed,
            'commercial_signature_id': self.commercial_id,
            'commercial_suivi_id': self.commercial_id, }

    def _contract_object(self, contract, contract_class=CommonContract):
        # from Dolibarr source, csm : contract status map
        csm = {
            '0': CommonContractStatus.draft,
            '1': CommonContractStatus.validated,
            '2': CommonContractStatus.closed, }
        # if origin is Dolibarr, key is None
        if contract[self.contract_key] is None:
            contract[self.contract_key] = CommonObject.common_id_of(contract['id'], self.server)
        d = {
            'InternalID': contract['id'],
            'ID': CommonContract.id_of(contract[self.contract_key]),
            'Origin': CommonContract.origin_of(contract[self.contract_key]),
            'Ref': contract['ref'],
            'Status': csm[contract['statut']],
            'Date_contract': self._rest.timestamp_to_date(contract['date_contrat'])}
        if contract.get('date_modification'):
            d['DateUpdate'] = self._rest.timestamp_to_datetime(contract['date_modification'])
        else:
            d['DateUpdate'] = self._rest.timestamp_to_datetime(contract['date_creation'])

        d[extra_fields_token] = self._rest.get_extra_fields(contract)
        c = contract_class(d)
        c.client = self._thirdparty_object(self.thirdparty(contract['fk_soc']))
        for s in contract['lines']:
            c.services.append(self._service_object(s, c.service_class))
        return c

    def get_contract_from_common_id(self, common_id, contract_class=CommonContract):
        """return contract object matching common id"""
        d = self._rest.get_from_key('contracts', self.contract_key, common_id)
        if d:
            return self._contract_object(d, contract_class)
        else:
            return None

    def add_contract(self, common_order, product_map, client_map):
        """
        create contract in Dolibarr and validate it
        """
        self.logger.info("New contract {0}".format(common_order.common_id()))
        dc_id = self._rest.call('POST', 'contracts', data=self._dol_new_contract(common_order, client_map))
        self.validate_contract(dc_id)
        for l in common_order.lines:
            # todo renew mention in product options
            date_start = date.today()
            date_end = date.today() + timedelta(days=365)
            l_id = self._rest.call(
                'POST', 'contracts/%d/lines' % dc_id,
                data=self._dol_new_contract_line(
                    product_map.get(l['product_id']), l['quantity'], date_start, date_end))
            self.activate_contract_line(dc_id, l_id, date_start, date_end)
        return dc_id

    def validate_contract(self, contract_id):
        """validate contract in dolibarr"""
        self._rest.call('POST', 'contracts/%d/validate' % contract_id, data={"notrigger": 0})

    def update_contract(self, common_contract):
        """todo: check and complete"""
        self.logger.info("Update contract {0}".format(common_contract.common_id()))
        # todo : to do
        values = {}
        if isinstance(common_contract, ExtraFields):
            self._rest.set_extra_fields(values, common_contract.get_extra_fields())
        return self._rest.call('PUT', 'contracts/'+common_contract.internal_ID, data=values)

    def all_contracts(self, contract_class):
        """return all contracts objects"""
        dc = self._rest.call('GET', 'contracts')
        if dc is None:
            return None
        for c in dc:
            yield self._contract_object(c, contract_class)

    # contract lines - services
    def _service_object(self, service, service_class=CommonService):
        ssm = {
            '0': CommonServiceStatus.initial,
            '4': CommonServiceStatus.running,
            '5': CommonServiceStatus.closed, }
        d = dict()
        d['InternalID'] = service['id']
        d['Origin'] = self.server
        d['Status'] = ssm[service['statut']]
        d['Ref'] = service['ref']
        d['Quantity'] = service['qty']
        d['Product_ref'] = service['product_ref']
        d['Date_start'] = self._rest.timestamp_to_date(service['date_start'])
        d['Date_end'] = self._rest.timestamp_to_date(service['date_end'])
        d[extra_fields_token] = self._rest.get_extra_fields(service)
        return service_class(d)

    def _dol_new_contract_line(self, product_id, quantity, date_start, date_end, extra_fields=None):
        d = {
            'fk_product': product_id,
            'status': CommonServiceStatus.running,
            'qty': quantity,
            'date_start': date_start,
            'date_end': date_end
        }
        self._rest.set_extra_fields(d, extra_fields)
        return d

    def _update_contract_line(self, contract_id, contract_line_id, _data):
        # todo : wait for bug resolution !!! https://github.com/Dolibarr/dolibarr/issues/9031
        dc = self._rest.get('contracts/%s' % contract_id)
        for l in dc['lines']:
            if l['id'] == contract_line_id:
                data_for_update = copy.deepcopy(l)
                data_for_update.update(copy.deepcopy(_data))
                if l.get('array_options'):
                    data_for_update['array_options'].update(copy.deepcopy(l['array_options']))
                if _data.get('array_options'):
                    data_for_update['array_options'].update(copy.deepcopy(_data['array_options']))
                self._rest.call('DELETE', 'contracts/%s/lines/%s' % (contract_id, contract_line_id))
                contract_line_id = self._rest.call('POST', 'contracts/%s/lines' % contract_id, data=data_for_update)
                if data_for_update['statut'] == '4':  # active contract
                    self.activate_contract_line(contract_id, contract_line_id,
                                                self._rest.timestamp_to_datetime(data_for_update['date_start']),
                                                self._rest.timestamp_to_datetime(data_for_update['date_end']))

    def update_contract_line(self, contract_id, contract_line_id, extra_fields):
        """
        updates extra fields of contract line
        :param contract_id:
        :param contract_line_id:
        :param extra_fields:
        :return:
        """
        self._update_contract_line(contract_id, contract_line_id, {'array_options': extra_fields})

    def activate_contract_line(self, contract_id, contract_line_id, date_start, date_end):
        """activates a contract line with start date and end date"""
        tms_start = time.mktime(date_start.timetuple())
        tms_end = time.mktime(date_end.timetuple())
        self._rest.call('PUT', 'contracts/%s/lines/%s/activate' % (contract_id, contract_line_id),
                        data={'datestart': tms_start, 'dateend': tms_end})

    def unactivate_contract_line(self, contract_id, contract_line_id):
        """unactivate contract line"""
        tms_start = time.mktime(datetime.now().timetuple())
        self._rest.call('PUT', 'contracts/%s/lines/%s/unactivate' % (contract_id, contract_line_id),
                        data={'datestart': tms_start})


if __name__ == '__main__':
    # run in cli mode
    d_api = dolibarr_REST.cli_mode()
