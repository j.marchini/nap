"""
Created on 2 mai. 2018
@author: jerome

"""

import yaml
import logging
import logging.handlers
import sendMail
from jinja2 import Template
import os


class NapFactory(object):
    """
        Instanciates the proper processus
        package name in yaml configuration file. p.e :
            package: "organization"
        class name in yaml configuration file. p.e :
            class: "Harmony"
        module name is same as class name with lower case initial. p.e : "harmony"
        the factory instanciates as this equivalent code :
            from organization.harmony import Harmony
            Harmony().run(args)
    """
    def __init__(self, args):
        with open(args.conf, 'r') as stream:
            yaml_conf = dict(yaml.load(stream)[0])
        nap_class_name = yaml_conf["class"]
        nap_module_name = "{0}.{1}{2}".format(yaml_conf["package"], nap_class_name.lower()[0], nap_class_name[1:])
        aux = __import__(nap_module_name, fromlist=[nap_class_name])
        nap_class = getattr(aux, nap_class_name)
        package_directory = os.path.join(os.path.dirname(__file__), yaml_conf["package"])
        nap_class(package_directory).run(args)


class NapProcessor(object):
    """
    base class for processors, abstract
    methods to be overloaded:
    do_process() do...
    """
    default_template_dir = 'template'

    def __init__(self, working_directory):
        self.args = None
        self.conf = {}
        self.working_directory = working_directory
        self.template_dir = ""
        # logger
        self.logger = logging.getLogger("FilterProcessorLogger")
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        self.formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        ch.setFormatter(self.formatter)
        self.logger.addHandler(ch)
        self.log_file = None
        self.no_mail = False
        self.SMTP_conf = None
        self.IMAP_conf = None
        self.alert = []
        self.activity = []
        self.activity_recipients = []
        self.alert_recipients = []
        self.from_address = ''

    def do_read_conf(self):
        """
        :return:
        """
        self.set_log_file(os.path.join(self.working_directory, self.conf.get("log_file", "nap.log")))
        self.template_dir = os.path.join(self.working_directory,
                                         self.conf.get("template_dir", self.default_template_dir))
        self.logger.info("Template directory : %s" % self.template_dir)
        self.no_mail = self.conf.get("no_mail", False)
        self.activity_recipients = self.conf.get("activity_recipients", [])
        self.alert_recipients = self.conf.get("alert_recipients", [])
        self.from_address = self.conf.get("from_address", "")
        self.SMTP_conf = self.conf.get("SMTP_conf")
        self.IMAP_conf = self.conf.get("IMAP_conf")

    def read_conf(self, conf):
        """ reading yaml configuration file"""
        with open(conf, 'r') as stream:
            try:
                self.conf = dict(yaml.load(stream)[0])
            except Exception as _E:
                self.logger.error('Error in YAML configuration file %s :' % conf)
                raise _E
            self.do_read_conf()

    def do_initialize(self):
        """ empty abstract method """
        pass

    def run(self, args):
        """main method"""
        # initialization
        self.args = args
        self.logger.setLevel(logging.WARNING)
        if self.args.verbose or self.args.test:
            self.logger.setLevel(logging.INFO)
        if self.args.debug:
                self.logger.setLevel(logging.DEBUG)
        self.logger.info("Running in verbose mode")
        self.logger.info("Reading configuration file : %s" % self.args.conf)
        self.read_conf(self.args.conf)
        self.do_initialize()
        # execution
        if self.args.test:
            self.test()
        else:
            self.process()

    def set_log_file(self, log_file_name):
        """prepare log file, rotating"""
        if log_file_name:
            self.log_file = log_file_name
            handler = logging.handlers.RotatingFileHandler(self.log_file, maxBytes=1_048_576, backupCount=5)
            handler.setFormatter(self.formatter)
            self.logger.addHandler(handler)

    # diagnostic and test
    def do_test(self):
        """
        testing connection configuration
        method to be overridden
        """
        self._test_smtp_connection()
        self._test_imap_connection()

    def test(self):
        """main test method, use do_test() to override"""
        self.logger.info("Running test mode")
        self.do_test()

    def _test_smtp_connection(self):
        if self.no_mail:
            self.logger.info("No mail specified in configuration, SMTP test connection skipped ")
            return

        self.logger.info("Test SMTP connection to %s" % self.SMTP_conf["host"])
        sendMail.test_connection(**self.SMTP_conf)
        self.logger.info("SMTP connection OK")

    def _test_imap_connection(self):
        if self.no_mail:
            self.logger.info("No mail specified in configuration, IMAP test connection skipped ")
            return

        self.logger.info("Test IMAP connection to %s" % self.IMAP_conf["host"])
        sendMail.draft_email(None, **self.IMAP_conf)
        self.logger.info("SMTP connection OK")

    def do_process(self):
        """main process method, to be overridden"""
        pass

    def process(self):
        """main process method, use do_process() to override"""
        self.activity = []
        self.alert = []
        try:
            self.logger.info("Start processing ....")
            self.do_process()
            self.logger.info(".... End processing")
        finally:
            self.report_alert()
            self.report_activity()

    # reporting : activity and alert
    def new_activity(self, msg):
        """add an activity to activity list and log"""
        self.activity.append(msg)
        self.logger.info(msg)

    def new_alert(self, msg):
        """add an alert to alert list and log"""
        self.alert.append(msg)
        self.logger.error(msg)

    def report_activity(self):
        """send activity list by mail"""
        if len(self.activity):
            self.send_email("NAP : activity report", "\n".join(self.activity), to=self.activity_recipients)

    def report_alert(self):
        """send alert list by mail"""
        if len(self.alert):
            self.send_email("NAP : alert report", "\n".join(self.alert), to=self.alert_recipients)

    # mailing draft and sending
    def send_email(self, subject, body, to=None, cc=None, bcc=None, files_to_attach=None):
        """compose and send an email"""
        email = sendMail.compose_email(subject, body, to, self.from_address, cc, bcc, files_to_attach)
        if not self.no_mail:
            sendMail.send_email(email, **self.SMTP_conf)

    def draft_email(self, subject, body, to=None, cc=None, bcc=None, files_to_attach=None):
        """compose and draft an email (put it in drafts folder of email account ready to be sent)"""
        email = sendMail.compose_email(subject, body, to, self.from_address, cc, bcc, files_to_attach)
        if not self.no_mail:
            sendMail.draft_email(email, **self.IMAP_conf)

    # templating
    def from_template(self, template_name, values):
        """ todo: doc"""
        with open(os.path.join(self.template_dir, template_name)) as f:
            template = Template(f.read())
            return template.render(values)
