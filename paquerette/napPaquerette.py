"""
Created on 2 mai. 2018
@author: jerome

Entities :

- Dolibarr Contract : "contract"
    - date contract
    - status: inactive, running, running expired, expired
    - options:
        - nap_category : managed, ""
        - alert_status :

- Dolibarr Contract Line : "account"
    - date start
    - date end
    - status : active, inactive
    - options :
        - server
        - instance
        - account
        - account_status :

"""

from datetime import datetime
# from enum import Enum

import dolibarr_API
import woocommerce_API
import nextcloud_API

import napTypes
from napUtils import date_past, iso_to_datetime
from dolibarr_WAPI import DolibarrWAPI
from napProcessor import NapProcessor


# contracts
class PaqueretteContract(napTypes.CommonContract, dolibarr_API.ExtraFields):
    """
    standard contract with extra fields
    nap_category : managed,
    alert_status : 30 days...
    """
    def __init__(self, definition):
        super().__init__(definition)
        dolibarr_API.ExtraFields.__init__(self, definition, ("nap_category", "alert_status"))
        self.service_class = PaqueretteService


# services - contract lines
class PaqueretteService(napTypes.CommonService, dolibarr_API.ExtraFields):
    """
    service enhanced to manage hosting services
    """
    def __init__(self, definition):
        super().__init__(definition)
        dolibarr_API.ExtraFields.__init__(self, definition, ("server", "instance", "account", "account_status"))


# products
class PaqueretteProduct(napTypes.CommonProduct, dolibarr_API.ExtraFields):
    """
    product with family, used to manage services and hosting contracts
    """
    def __init__(self, definition):
        super().__init__(definition)
        dolibarr_API.ExtraFields.__init__(self, definition, ("family",))


class NapPaquerette(NapProcessor):
    """nap processor for Paquerette"""
    def __init__(self, working_directory):
        super().__init__(working_directory)
        self.dolibarr_API = dolibarr_API.DolibarrApi()
        self.dolibarr_WAPI = DolibarrWAPI()
        self.NEXTCLOUD_link = None
        self.WOOCOMMERCE_link = None
        # from woocommerce to dolibarr
        self.client_map = {}
        self.product_map = {}
        self.back_office = 'contact@paquerette.eu'

    def do_test(self):
        """testing paquerette connection settings"""
        super().do_test()
        self.dolibarr_API.test_connection()
        self.NEXTCLOUD_link.test_connection()
        # self.WOOCOMMERCE_link.test_connection()

    def do_initialize(self):
        """apply configuration"""
        super().do_initialize()
        self.dolibarr_API.initialize(self.logger, self.conf.get("dolibarr_conf"))
        self.dolibarr_WAPI.initialize(self.logger, self.conf.get('dolibarr_WAPI_conf'))
        self.NEXTCLOUD_link = nextcloud_API.NextcloudAPI(self.logger, self.conf.get("nextcloud_conf"))
        self.WOOCOMMERCE_link = woocommerce_API.WoocommerceApi(self.logger, self.conf.get("woocommerce_conf"))
        self.WOOCOMMERCE_link.initialize()
        self.client_map = {}
        self.product_map = {}

    def do_process(self):
        """main process method for paquerette"""
        super().do_process()
        # # self.synchronize_clients()
        # # self.check_products()
        # # self.synchronize_orders()
        # self.review_contracts()
        self.review_customer_invoices()

    def synchronize_clients(self):
        """
        todo: to be re implemented using dolibarrWAPI
        :return:
        """
        def more_recent(woocommerce_client, dolibarr_client):
            """
            true if woocommerce client is mode recent than dolibarr client,
            based on builtin updates timestamps, applying dolibarr timezone
            """
            if dolibarr_client.date_maj is None:
                return woocommerce_client.date_update > dolibarr_client.date_create
            else:
                return woocommerce_client.date_update > \
                       iso_to_datetime(dolibarr_client.date_maj).replace(tzinfo=self.dolibarr_API.timezone)

        woocommerce_clients = self.WOOCOMMERCE_link.list_clients()
        for wc in woocommerce_clients:
            dc = self.dolibarr_API.get_client_from_common_id(wc.common_id(), napTypes.CommonCustomer)
            if dc:
                self.client_map[wc.internal_ID] = dc.internal_ID
            else:
                try:
                    ndc_id = self.dolibarr_API.add_client(wc)
                    self.client_map[wc.internal_ID] = ndc_id
                    continue
                except dolibarr_API.DolibarrApiException as e:
                    self.logger.error("Error with client %s" % wc.internal_ID)
                    self.logger.error(e)
                    continue
            if more_recent(wc, dc):
                dc.date_maj = datetime.now().isoformat()
                self.dolibarr_API.update_client(dc, wc)

    def check_products(self):
        """
        alert when woocommerce product is not referenced in dolibarr
        :return:
        """
        woocommerce_products = self.WOOCOMMERCE_link.list_products()
        for wp in woocommerce_products:
            dp = self.dolibarr_API.product_by_ref(wp.key)
            if dp:
                self.product_map[wp.ID] = dp.ID
            else:
                self.logger.error(
                    'Woocommerce product "{0}" doesn\'t exists in Dolibarr with key "{1}"'.format(wp.name, wp.key))
    # def check_nextcloud_product(self, product):
    #     if product.name is None or product.family is None or product.options is None:
    #         self.new_alert('Product "{0}" is not complete, all fields must be filled'.format(product.key))
    #

    def synchronize_orders(self):
        """
        todo:
        :return:
        """
        woocommerce_orders = self.WOOCOMMERCE_link.list_orders()
        for wo in woocommerce_orders:
            dc = self.dolibarr_API.get_contract_from_common_id(wo.common_id())
            if not dc:
                self.new_activity('New contract for order {0}'.format(wo.common_id()))
                self.dolibarr_API.add_contract(wo, self.product_map, self.client_map)

    def review_contracts(self):
        """
        main method
        check all contracts intended to be managed by nap
        :return:
        """
        for contract in self.dolibarr_API.all_contracts(PaqueretteContract):
            if contract.nap_category is None or contract.nap_category.upper() != "MANAGED":
                continue
            self.check_contract(contract)
            self.review_nextcloud_services(contract)

    def check_contract(self, contract):
        """
        check dates and draft mails for renewal
        :param contract:
        :return:
        """
        def message_contract_end(_contract, date_end, delay):
            """draft mail using templates for renewal """
            if date_past(date_end, -delay) and (_contract.alert_status is None or int(_contract.alert_status) < -delay):
                self.new_activity('Alert for contract {0} {1} days from end'.format(_contract.ref, -delay))
                _contract.alert_status = -delay
                self.dolibarr_API.update_contract(_contract)
                # todo: check date update
                self.draft_email(
                    self.from_template('Mail_contract_end_coming_title.txt', {"delay": delay}),
                    self.from_template('Mail_contract_end_coming.txt',
                                       {"contract_ref": _contract.ref,
                                        "delay": delay,
                                        }),
                    to=contract.client.e_mail)

        services_end_date = None
        for service in contract.services:
            # start date is mandatory
            if service.date_start is None:
                self.new_alert('Contract "{0}" Service "{1}" Start date is Null'.format(contract.ref, service.ref))
            # if running, end date is mandatory
            if service.status == napTypes.CommonServiceStatus.running and service.date_end is None:
                self.new_alert('Contract "{0}" Service "{1}" End date is Null'.format(contract.ref, service.ref))
            # if initial state for more than one month
            if service.status == napTypes.CommonServiceStatus.initial and date_past(service.date_start, 30):
                self.new_alert(
                    'Contract "{0}" Service "{1}" Start date is past from 30 days and service still in initial state'.
                    format(contract.ref, service.ref))
            if services_end_date is None or services_end_date > service.date_end:
                services_end_date = service.date_end
        message_contract_end(contract, services_end_date, 30)
        message_contract_end(contract, services_end_date, 7)

    # nextcloud
    def review_nextcloud_services(self, contract):
        """
        for each nextcloud service :
            creates account if needed
            draft alert for renewal

        :param contract:
        :return:
        """
        for service in contract.services:
            p = self.dolibarr_API.product_by_ref(service.product_ref, PaqueretteProduct)
            if p is None:
                continue
            if p.family != "Nextcloud":
                continue
            # only running services are managed here
            if service.status != napTypes.CommonServiceStatus.running:
                continue
            # if service.extended_status == CommonServiceExtendedStatus.not_managed:
            #     continue
            if service.account_status != "":
                self.check_nextcloud_service(contract, service)
            if service.server is None:
                self.new_nextcloud_account(contract, service, p)
                continue
            # d : red alert
            # if date.today() >= service.date_end and \
            #         service.extended_status in [CommonServiceExtendedStatus.running,
            #                                     CommonServiceExtendedStatus.yellow_alert,
            #                                     CommonServiceExtendedStatus.orange_alert,
            #                                     ]:
            #     self.new_activity('red alert for contract {0} service {1}'.format(contract.Ref, service.ref))
            #     self.dolibarr_API.update_contract_line_extended_status(
            #         contract.ID, service.ID, napTypes.CommonServiceExtendedStatus.red_alert)
            #     self.draft_email(
            #         "Votre abonnement Nextcloud expire aujourd'hui",
            #         self.from_template('Mail_nextcloud_account_red.txt',
            #                            {"server": service.server,
            #                             "account": service.account,
            #                             }),
            #         to=[contract.Client.e_mail])
            #     continue
            # d + 2 account suspended
            # if date.today() >= service.date_end + timedelta(days=2) and \
            #         service.extended_status in [napTypes.CommonServiceExtendedStatus.running,
            #                                     napTypes.CommonServiceExtendedStatus.yellow_alert,
            #                                     napTypes.CommonServiceExtendedStatus.orange_alert,
            #                                     napTypes.CommonServiceExtendedStatus.red_alert,
            #                                     ]:
            #     self.new_activity('suspension of account {3}@{2} for contract {0} service {1}'.format(
            #         contract.ref, service.ref, service.server, service.account))
            #     self.dolibarr_API.update_contract_line_extended_status(
            #         contract.ID,
            #         service.ID,
            #         napTypes.CommonServiceExtendedStatus.suspended)
            #     self.draft_email(
            #         'Votre compte nextcloud a été suspendu',
            #         self.from_template('Mail_account_suspended.txt',
            #                            {"server": service.server,
            #                             "account": service.account,
            #                             }),
            #         to=[contract.Client.e_mail])
            #     continue
            # d + 30 account closed
            # if date.today() >= service.date_end + timedelta(days=30) and \
            #         service.extended_status in [napTypes.CommonServiceExtendedStatus.running,
            #                                     napTypes.CommonServiceExtendedStatus.yellow_alert,
            #                                     napTypes.CommonServiceExtendedStatus.orange_alert,
            #                                     napTypes.CommonServiceExtendedStatus.red_alert,
            #                                     napTypes.CommonServiceExtendedStatus.suspended,
            #                                     ]:
            #     self.new_activity('closure of account {3}@{2} for contract {0} service {1}'.format(
            #         contract.ref, service.ref, service.server, service.account))
            #     self.dolibarr_API.update_contract_line_extended_status(
            #         contract.ID,
            #         service.ID,
            #         napTypes.CommonServiceExtendedStatus.closed)
            #     self.draft_email(
            #         'Votre compte nextcloud a été fermé',
            #         self.from_template('Mail_account_closed.txt',
            #                            {"server": service.server,
            #                             "account": service.account,
            #                             }),
            #         to=[contract.Client.e_mail])
            #     continue

    def new_nextcloud_account(self, contract, service, product):
        """creates account, draft email"""
        display_name = contract.client.first_name + ' ' + contract.client.name
        email = contract.client.e_mail
        user_id, user_password = self.NEXTCLOUD_link.new_client_account(display_name, email, product.options)
        self.new_activity('New Nextcloud account {0} identified by {1}'.format(user_id, user_password))
        # todo: review
        # self.dolibarr_API.update_contract_line(contract.internal_ID, service.ID, self.NEXTCLOUD_link.server, user_id)
        self.draft_email(
            'Message de Pâquerette : votre compte Nextcloud est activé',
            self.from_template('Mail_nextcloud_account_green.txt',
                               {"server": service.server,
                                "user_id": user_id,
                                "user_password": user_password}),
            to=[email])
        return user_id

    def check_nextcloud_service(self, contract, service):
        """
        send alert to admin if dolibarr record are missing or incomplete
        :param contract:
        :param service:
        :return:
        """
        if service.extended_status is None:
            self.new_alert('Contract "{0}" Service "{1}" extended status is Null'.format(contract.ref, service.ref))
        if service.server is None:
            self.new_alert('Contract "{0}" Service "{1}" server is Null'.format(contract.ref, service.ref))
        if service.account is None:
            self.new_alert('Contract "{0}" Service "{1}" account is Null'.format(contract.ref, service.ref))

    def review_customer_invoices(self):
        """
        watch coopaname process and payments
        :return:
        """
        for invoice in self.dolibarr_API.all_invoices(napTypes.CommonCustomerInvoice):
            if invoice.customer_ref is None:
                self.dolibarr_WAPI.update_invoice_customer_ref(invoice.ID, 'TEST')
                # self.send_email(
                #     'facture à saisir dans coopaname',
                #     self.from_template('Mail_customer_invoice_coopaname', {'invoice': invoice}),
                #     to=self.back_office)
                # self.draft_email(
                #     'facture à saisir dans coopaname',
                #     self.from_template('Mail_customer_invoice', {'invoice': invoice}),
                #     to=invoice.client.e_mail)
                pass
